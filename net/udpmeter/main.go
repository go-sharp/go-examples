package main

import (
	"fmt"
	"log"
	"net"
)

func main() {
	srvConn, err := net.ListenUDP("udp", &net.UDPAddr{IP: net.ParseIP("127.0.0.1"), Port: 5000})
	if err != nil {
		log.Fatal(err)
	}

	defer srvConn.Close()
	go func() {
		buf := make([]byte, 60)
		for {

			_, _, err := srvConn.ReadFromUDP(buf)
			if err != nil {
				log.Println(err)
			}
			fmt.Printf("%v", string(buf))
		}
	}()

	fmt.Scanln()

}
