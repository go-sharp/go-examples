package main

import (
	"fmt"
	"os"
	"strconv"
	"sync"

	"bitbucket.org/go-sharp/slog"
	"bitbucket.org/go-sharp/slog/sinks"
)

var logger slog.Logger

var numChan = 1000
var numGoroutine = 10000

func init() {
	logger = slog.NewLoggerConfig(sinks.NewConsoleSink()).
		CreateLogger()
}

func producer(id int, ch chan<- string) {
	for i := 0; i < 1000; i++ {
		ch <- fmt.Sprintf("Sender goroutine %v with message id %v", id, i)
	}
}

func receiver(id int, ch <-chan string) {
	for msg := range ch {
		logger.Info("{Message} -> received by goroutine {ID}", msg, id)
	}
}

func main() {
	if len(os.Args) == 2 {
		numChan, _ = strconv.Atoi(os.Args[1])
		numGoroutine, _ = strconv.Atoi(os.Args[2])
	}

	fmt.Printf("Using %v channels and %v goroutines\nPress any key to start...", numChan, numGoroutine)
	fmt.Scanln()

	chans := make([]chan string, numChan)
	for i := 0; i < numChan; i++ {
		chans[i] = make(chan string, 0)
	}

	fmt.Println("Starting senders ...")

	var wgs sync.WaitGroup
	// Startup sender goroutines
	wgs.Add(numGoroutine / 2)
	for i := 1; i <= numGoroutine/2; i++ {
		go func(id int) {
			producer(id, chans[id%numChan])
			wgs.Done()
		}(i)
	}

	fmt.Println("Starting receivers ...")

	var wgr sync.WaitGroup
	// Startup receivers
	wgr.Add(numGoroutine / 2)
	for i := numGoroutine/2 + 1; i <= numGoroutine; i++ {
		go func(id int) {
			receiver(id, chans[id%numChan])
			wgr.Done()
		}(i)
	}

	// Wait for all sender to finish
	wgs.Wait()
	for i := range chans {
		close(chans[i])
	}
	fmt.Println("Waiting receiver to finish")
	wgr.Wait()

}
