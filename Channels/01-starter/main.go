package main

import (
	"fmt"
)

func main() {
	// Create a channel of ints
	ch := make(chan int)

	// Spin up a goroutine and send values into the channel
	go func() {
		for i := 0; i <= 100; i++ {
			ch <- i
		}
		close(ch) // Close the channel and signaling the receiver that all values has been sent
	}()

	// Iterate over the channel and print the values out
	for i := range ch {
		fmt.Println(i)
	}
}
